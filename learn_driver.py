"""
Initialize MNIST dataset for self-organized learning and display feed
forward network weight and state details
"""

import learn_dataset as ld
import learn_io as io
import learn_ops as op
import numpy as np


# Data store for network activation and weight values
_data_dict = io.DataDict(io.SAVE_PATH)


class SubnetLayer(object):
    """Layer consisting of multiple node populations"""
    def __init__(self, input_dim, node_dim):
        inhibit_dim = node_dim // 2  # Default inhibitory node count
        self.hidden = SubnetNodes(input_dim, node_dim)
        self.recurrent = SubnetNodes(node_dim, inhibit_dim)
        self.output = SubnetNodes(inhibit_dim, node_dim)
        # Store list of node populations for data processing
        self.nodes_list = [('hidden', self.hidden),
                           ('recurrent', self.recurrent),
                           ('output', self.output)]
        x = np.zeros(input_dim)  # Empty input on initialization
        self.record(x)  # Record initial starting state

    def forward(self, x, lr=0.01, pr=0.10):
        """Perform a forward propagation learning iteration"""
        h_state = self.hidden.excite(x)
        self.recurrent.excite(self.hidden.activations)
        self.output.inhibit(self.recurrent.activations, h_state)
        # Update node population weight values based on output activations
        self.hidden.update(x, lr, pr)
        self.recurrent.update(self.hidden.activations, lr, pr)
        self.output.update(self.recurrent.activations, lr, pr)
        self.record(x)  # Record post-update data

    def record(self, x):
        """Dump subnet population data to data dictionary"""
        _data_dict.append(('input', 'activations'), np.copy(x))
        for nodes_id, nodes_obj in self.nodes_list:
            nodes_obj.record(nodes_id)


class SubnetNodes(object):
    """Population of nodes with similar locality and connectivity"""
    def __init__(self, input_dim, node_dim):
        self.activations = np.zeros(node_dim)
        self.potentials = np.random.rand(node_dim)
        self.weights = np.random.rand(input_dim, node_dim)

    def excite(self, x):
        """Forward propagate signal x through weight matrix w"""
        weighted_sum = np.dot(x, self.weights)
        self.activations = activation(weighted_sum, self.potentials)
        return weighted_sum

    def inhibit(self, actvs, state_sum):
        """Apply weighted inhibition signal activations to state_sum"""
        inhibit_factor = self.weights.shape[1] / self.weights.shape[0]
        inhibit_sum = np.dot(actvs, self.weights) * inhibit_factor
        state_sum = state_sum - inhibit_sum
        self.activations = activation(state_sum, self.potentials)

    def record(self, nodes_id):
        """Dump node data to individual data dict collections"""
        _data_dict.append((nodes_id, 'activations'), np.copy(self.activations))
        _data_dict.append((nodes_id, 'potentials'), np.copy(self.potentials))
        _data_dict.append((nodes_id, 'weights'), np.copy(self.weights))

    def update(self, actvs, lr, pr):
        """Update the weight matrix with noisy decay and growth"""
        actvs = actvs.reshape(actvs.shape[0], 1)
        weighted_actvs = np.multiply(actvs, self.weights)
        input_dim, node_dim = self.weights.shape[0], self.weights.shape[1]
        growth_mod = input_dim / np.sum(self.weights, axis=0)
        p_squared = self.potentials ** 2
        learn_noise = np.random.rand(input_dim, node_dim) * lr
        growth_update = weighted_actvs * p_squared * growth_mod
        decay_update = p_squared * self.weights
        self.weights += (growth_update - decay_update) * learn_noise
        self.potentials += np.random.rand(node_dim) * pr
        self.potentials = self.potentials * (self.activations < 1)


def activation(a, p):
    """Return a binary matrix representing node activations"""
    return (a * p >= 1).astype(int)


def activation_norm(x):
    """Return a normalized binary input array"""
    x = x / np.max(x)
    return activation(x, 2)


def learn_mnist(max_samples, hidden_nodes=10, window_dims=(5, 5)):
    mnist_data = ld.MNISTDataset(io.SAVE_PATH + 'mnist.dat')
    img_dims, sample_count = mnist_data.data_dims, 1
    subnet_layer = SubnetLayer(window_dims[0] * window_dims[1], hidden_nodes)
    for sample_data, sample_label in mnist_data:
        sample_data = sample_data.flatten()  # Convert to vector
        for w_tuple in op.sliding_window(img_dims, sample_data, 3, window_dims):
            if sample_count >= max_samples:
                return subnet_layer
            x, x_idx, y_idx = w_tuple  # Window indices and features
            if np.sum(x) > 0:  # If the input window is not empty...
                subnet_layer.forward(activation_norm(x))  # Forward propagate
                sample_count += 1
    return subnet_layer


if __name__ == '__main__':
    print('-- START DEBUG --')
    subnet = learn_mnist(100)  # Process 100 window samples
    _data_dict.to_disk('net_data', pickle_file=True)
    print('-- END DEBUG --')
