"""
Periodically print progress to console
Available from https://gist.github.com/aubricus
"""

import sys


# Print iterations progress
def print_iterations(iteration, message=None, refresh_rate=10):
    if message is None:
        message = 'Processing: '
    if iteration % refresh_rate == 0:
        message = '\r' + message + ' iteration = %s' % (iteration,)
        print(message, end='', flush=True)


# Call after final iteration
def print_flush():
    sys.stdout.write('\n')
    sys.stdout.flush()


# Print iterations progress
def print_progress(iteration, total, prefix='', suffix='', decimals=1, bar_length=100):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        bar_length  - Optional  : character length of bar (Int)
    """
    str_format = "{0:." + str(decimals) + "f}"
    percents = str_format.format(100 * (iteration / float(total)))
    filled_length = int(round(bar_length * iteration / float(total)))
    bar = '|' * filled_length + '-' * (bar_length - filled_length)
    sys.stdout.write('\r%s |%s| %s%s %s' % (prefix, bar, percents, '%', suffix)),
    '''
    if iteration == total:
        sys.stdout.write('\n')
    sys.stdout.flush()'''