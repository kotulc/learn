"""
Word training datasets and word vectors for use in word_model.py
"""

import mnist
import learn_io as io


class MNISTDataset(object):
    """benchmark dataset for simple computer vision learning"""
    def __init__(self, data_path):
        load_success = self.load_dataset(data_path)
        if not load_success:  # Normalize test and train data
            test_x, train_x = mnist.test_images(), mnist.train_images()
            self.data_dims = (test_x.shape[1], test_x.shape[2])
            self.test_data = (test_x * (1 / 255), mnist.test_labels())
            self.train_data = (train_x * (1 / 255), mnist.train_labels())
            io.save_pickle(data_path, (self.test_data, self.train_data))

    def __getitem__(self, idx):
        return self.train_data[0][idx], self.train_data[1][idx]

    def __len__(self):
        return len(self.train_data[0])

    def load_dataset(self, data_path):
        """Load this object from data_path"""
        data_obj = io.load_pickle(data_path)
        if data_obj is None:
            return False
        self.test_data, self.train_data = data_obj
        data_sample = self.test_data[0]
        self.data_dims = (data_sample.shape[1], data_sample.shape[2])
        return True
