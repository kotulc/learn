"""
Operations relating to numpy matrix manipulation
"""


def sliding_window(image_dims, image_vector, step_size, window_dims):
    """Extract flattened square window_size windows from image_vector"""
    window_list = []  # Return a list of window_indices, window_values
    ix_dim, iy_dim = image_dims  # Assuming 2d (x, y) image dimensions
    wx_dim, wy_dim = window_dims  # Assuming 2d (x, y) window dimensions
    x_stop, y_stop = (ix_dim - wx_dim) + 1, (iy_dim - wy_dim) + 1
    image_matrix = image_vector.reshape(ix_dim, iy_dim)  # Vector to matrix
    for x_idx in range(0, x_stop, step_size):
        for y_idx in range(0, y_stop, step_size):
            # Extract each window starting at x_idx, y_idx
            image_window = image_matrix[x_idx:x_idx + wx_dim, y_idx:y_idx + wy_dim]
            window_list.append((image_window.flatten(), x_idx, y_idx))
    return window_list

