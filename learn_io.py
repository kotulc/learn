"""
Input and output operations and related helper functions
"""

import gzip
import json
import numpy
import os.path
import pandas as pd
import pickle


# Helper data path constants
DIR_PATH = os.path.dirname(os.path.realpath(__file__))
SAVE_PATH = DIR_PATH + '\\data\\'


class DataDict(object):
    """A single dictionary for single and multi-indexed Pandas DataFrames"""
    def __init__(self, data_dir):
        os.makedirs(os.path.dirname(data_dir), exist_ok=True)
        self.data_dir = data_dir
        self.data_dict = {}

    def append(self, dict_key, dict_value):
        """Append dict_value to the list at dict_key"""
        if dict_key not in self.data_dict:
            self.data_dict[dict_key] = [dict_value]
        else:
            self.data_dict[dict_key].append(dict_value)

    def to_disk(self, file_name, pickle_file=True):
        if pickle_file:
            file_name += '.pkl'
            pd.DataFrame(self.data_dict).to_pickle(self.data_dir + file_name)
        else:
            file_name += '.csv'
            pd.DataFrame(self.data_dict).to_csv(self.data_dir + file_name)


class DataStore(object):
    """A collection of related data dicts in a nested directory"""
    def __init__(self, store_dir):
        os.makedirs(os.path.dirname(store_dir), exist_ok=True)
        self.store_dir = store_dir
        self.data_collections = {}
        self.data_dicts = {}

    def __getitem__(self, dict_id):
        return self.data_dict(dict_id)

    def append(self, data_dict, dict_id):
        """Append all key value pairs from data_dict to the store dict"""
        store_dict = self.data_dict(dict_id)
        for k, v in data_dict.items():
            if k not in store_dict:  # Initialize new list
                store_dict[k] = [v]
            else:  # Existing key, append to value list
                store_dict[k].append(v)

    def collection(self, c_id):
        """Return a child DataStore object at the sub directory c_id"""
        if c_id not in self.data_collections:
            store_path = self.store_dir + c_id + '\\'
            self.data_collections[c_id] = DataStore(store_path)
        return self.data_collections[c_id]

    def data_dict(self, dict_id):
        """Return a dict object associated with dict_id"""
        if dict_id not in self.data_dicts:
            self.data_dicts[dict_id] = {}
        return self.data_dicts[dict_id]

    def save_meta(self, meta_obj):
        """Save a meta descriptor object to the root directory"""
        save_json(self.store_dir + 'meta.json', meta_obj)

    def to_disk(self):
        """Save all dictionaries as DataFrame objects"""
        for ds in self.data_collections.values():
            ds.to_disk()
        for dict_id, dict_obj in self.data_dicts.items():
            df_path = self.store_dir + dict_id + '.pkl'
            pd.DataFrame(dict_obj).to_pickle(df_path)


def load_or_build(build_fcn, build_args, local_path, report=True):
    """Return file from file_path if found, else build with build_fcn"""
    file_path = SAVE_PATH + local_path
    if report:
        print(build_fcn.__name__.upper() + ': loading...', end='', flush=True)
    if file_path.endswith('.json'):
        file_obj = load_json(file_path)
        if file_obj is None:
            file_obj = build_fcn(*build_args)
            save_json(file_path, file_obj)
    else:  # Assume pickle file has been compressed
        file_obj = load_pickle(file_path, True)
        if file_obj is None:
            file_obj = build_fcn(*build_args)
            save_pickle(file_path, file_obj, True)
    if report:
        print('\r' + build_fcn.__name__.upper() + ': operation complete.')
    return file_obj


def load_json(file_str):
    if os.path.isfile(file_str):
        with open(file_str, 'r') as file_obj:
            json_obj = json.load(file_obj)
        return json_obj
    return None


def load_pickle(file_str, compressed=True):
    if os.path.isfile(file_str):
        if compressed:
            with gzip.open(file_str, 'rb') as file_obj:
                pkl_obj = pickle.load(file_obj)
        else:
            with open(file_str, 'rb') as file_obj:
                pkl_obj = pickle.load(file_obj)
        return pkl_obj
    return None


def load_text(file_str):
    if os.path.isfile(file_str):
        with open(file_str, 'r') as file_obj:
            lines = file_obj.readlines()
        return lines
    return None


def load_vectors(vect_path):
    if os.path.isfile(vect_path):
        vect_dict = {}
        with open(vect_path, 'r', encoding="utf8") as file_obj:
            for line in file_obj:
                split_line = line.split()
                vect_key = split_line[0]
                vect_value = numpy.array([float(val) for val in split_line[1:]])
                vect_dict[vect_key] = vect_value
        return vect_dict
    else:
        raise FileNotFoundError


def save_json(file_str, data_obj):
    os.makedirs(os.path.dirname(file_str), exist_ok=True)
    with open(file_str, 'w') as file_obj:
        json.dump(data_obj, file_obj)


def save_pickle(file_str, data_obj, compressed=True):
    os.makedirs(os.path.dirname(file_str), exist_ok=True)
    if compressed:
        with gzip.open(file_str, 'wb') as file_obj:
            pickle.dump(data_obj, file_obj, protocol=-1)
    else:
        with open(file_str, 'wb') as file_obj:
            pickle.dump(data_obj, file_obj, protocol=-1)